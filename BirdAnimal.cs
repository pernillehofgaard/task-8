﻿namespace Task8
{
	public class BirdAnimal : Animal, IFlyer, IWalker //I Guess birds can walk/run
	{
		public void landing()
		{
			System.Console.WriteLine("Mayday, black hawk down!");
		}

		public void Run()
		{
			System.Console.WriteLine("Bird is running away with your fries");
		}

		public void takeoff()
		{
			System.Console.WriteLine("Ready for takeoff");
		}

		public void Walk()
		{
			System.Console.WriteLine("Bird is walking towards your food");
		}
	}
}
