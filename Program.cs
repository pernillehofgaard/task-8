﻿using System;

namespace Task8
{
	class Program
	{
		static void Main(string[] args)
		{
			CatAnimals cat = new CatAnimals();
			DogAnimals dog = new DogAnimals();
			BirdAnimal hawk = new BirdAnimal();

			cat.Walk();
			cat.Run();
			Console.WriteLine();

			dog.Run();
			dog.Walk();
			Console.WriteLine();

			hawk.Walk();
			hawk.Run();
			hawk.takeoff();
			hawk.landing();
		}
	}
}
