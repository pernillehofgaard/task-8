﻿namespace Task8
{
	public class DogAnimals : Animal, IWalker
	{
		public void Run()
		{
			System.Console.WriteLine("Dog animal is running");
		}

		public void Walk()
		{
			System.Console.WriteLine("Dog animal is walking");
		}
	}
}
