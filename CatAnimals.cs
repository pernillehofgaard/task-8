﻿namespace Task8
{
	public class CatAnimals : Animal, IWalker
	{
		public void Run()
		{
			System.Console.WriteLine("Cat animal is running");
		}

		public void Walk()
		{
			System.Console.WriteLine("Cat animal is walking");
		}
	}
}
