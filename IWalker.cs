﻿namespace Task8
{
	public interface IWalker {
		
		public void Walk();
		public void Run();
		
	}
}
